function instance = sql_getInstance()
%SQL_GETINSTANCE Get the default instance if the instance.txt file exists

    p = fileparts(mfilename('fullpath'));
    p = fullfile('instance.txt');
    
    if exist(p, 'file')
        instance = fileread(p);
    else
        instance = [];
    end

end
