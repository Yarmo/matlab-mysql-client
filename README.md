# MATLAB-MySQL-CLIENT

## General information

This is a MySQL client for Matlab that supports both MySQL and MariaDB, and should work with most SQL-like databases.

## Usage

### Preparation

Install the ODBC driver built for your database engine and configure it accordingly.

### Specify the default ODBC instance (instance.txt)

It's useful to create a file named 'instance.txt' with the name of the default ODBC instance for easier use of sql_query.

### Inside matlab

Make sure this folder is added to PATH using addpath.

Run `sql_query(query)` to use this tool.

Run `help sql_query` for more information and details.
