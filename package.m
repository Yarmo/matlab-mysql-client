function info = package()
%PACKAGE Get matpack package info

    info = struct;
    
    info.name = 'matlab-mysql-client';
    info.description = 'Simple & slim MySQL client for Matlab';
    info.version = '0.2.0';
    info.author = 'Yarmo Mackenbach';
    
    info.paths = {'/'};
    
    info.dependencies = {};

end

