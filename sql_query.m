function [data status] = sql_query(query, varargin)
%SQL_QUERY Query a database
%   data = sql_query(query) runs the provided query against the default 
%   database specified in the instance.txt file and returns the struct
%   data with the results
%   
%   sql_query(..., 'instance', instance) uses the provided (ODBC) instance
%   to run the query.
%
%   [data status] = sql_query(...) also returns the status struct (see
%   below). This will prevent sql_query from throwing error. See below if
%   you still want errors with two output parameters
%   
%   ABOUT ERRORS (TWO OUTPUT PARAMETERS ONLY)
%   =========================================
%   As this function is mainly intended to be used in the background of
%   more complicated processes, it is less desirable to throw errors that
%   would stop these processes. For almost any error, sql_query will fail
%   silently and return empty. However, the second output argument (status)
%   will still let the user know if something went wrong (status.success)
%   and if so, what (status.message). It's then easy to throw an error
%   manually if that is desired behavior:
%       if ~status.success, error(status.message); end
%   
%   Note: with a single output parameter, errors are thrown as usual.
%
%   See also sql_generateQuery
    
    % Initialisation
    p = inputParser();
    p.addRequired('query');
    p.addParamValue('instance', '', @ischar);
    p.parse(query, varargin{:});
    p = p.Results;
    
    % Handle output
    data = [];
    
    % Status object
    status = struct;
    status.query = p.query;
    status.success = 0;
    status.error = '';
    
    % Silent error
    silent = 0;
    if nargout > 1
        silent = 1;
    end
    
    % If no database is provided, load the default from the config.ini
    if isempty(p.instance)
        p.instance = sql_getInstance;
        if isempty(p.instance)
            status.error = 'No instance provided or set via instance.txt';
            return;
        end
    end
    
    % Transform the query if it comes from generateSQLQuery
    if isstruct(p.query)
        if isfield(p.query, 'query')
            p.query = p.query.query;
        else
            status.error = 'No valid query provided';
            return;
        end
    end
    
    % Make the connection
    conn = database(p.instance, '', '');
    
    % Error handling
    status = p_handleError(status, silent, conn, 'connection_error', {});
    
    % Create a query cursor
    curs = exec(conn, p.query);
    
    % Error handling
    status = p_handleError(status, silent, curs, 'cursor_error', {conn});
    
    % Run the query
    curs = fetch(curs);
    
    % Error handling
    status = p_handleError(status, silent, curs, 'query_error', {conn curs});
    
    % Extract the data
    data = curs.Data;
    
    % Transform data
    if iscell(data)
        % Return empty if no data was returned
        if numel(data) == 1 && strcmp(data{1}, 'No Data')
            data = [];
        else
            % Convert the database result from a cell matrix to struct
            columnDataStr = columnnames(curs);                  % get columnnames from cursor object
            columnDataStr = strrep(columnDataStr, '(', '_');    % replace illegal characters
            columnDataStr = strrep(columnDataStr, ')', '_');    % replace illegal characters
            columnDataStr = strrep(columnDataStr, '*', 'x');    % replace illegal characters
            columnDataStr = strrep(columnDataStr, '''', '');    % remove single quotes
            columnData = p_strsplit(columnDataStr, ',');        % convert to cell array
            data = cell2struct(data, columnData', 2);
        end
    else
        data = [];
    end
    
    % Close cursor
    close(curs);
    
    % Close connection
    close(conn);
    
    % Update status
    status.success = 1;
	
end

function status = p_handleError(status, silent, obj, id, conn)
    % Determine if error has happened
    msg = '';
    try
        if ~isempty(obj.message)
            msg = obj.message;
        end
    catch err
    end
    try
        if ~isempty(obj.Message)
            msg = obj.Message;
        end
    catch err
    end
    
    if isempty(msg)
        return;
    end
    
    % Exceptional case: when no data is returned as expected => no error
    if isequal(msg, 'Invalid Result Set')
        return;
    end
    
    % Error has happened
    % Close all connections and cursors
    for iConn = 1:numel(conn)
        close(conn{iConn});
    end
    % Handle error silently or throw exception
    if silent
        status.error = msg;
        return;
    else
        error(['sql_query:' id], msg);
    end
end

function terms = p_strsplit(s, delimiter)
%STRSPLIT Splits a string into multiple terms
%
%   terms = strsplit(s)
%       splits the string s into multiple terms that are separated by
%       white spaces (white spaces also include tab and newline).
%
%       The extracted terms are returned in form of a cell array of
%       strings.
%
%   terms = strsplit(s, delimiter)
%       splits the string s into multiple terms that are separated by
%       the specified delimiter. 
%   
%   Remarks
%   -------
%       - Note that the spaces surrounding the delimiter are considered
%         part of the delimiter, and thus removed from the extracted
%         terms.
%
%       - If there are two consecutive non-whitespace delimiters, it is
%         regarded that there is an empty-string term between them.         
%
%   Examples
%   --------
%       % extract the words delimited by white spaces
%       ts = strsplit('I am using MATLAB');
%       ts <- {'I', 'am', 'using', 'MATLAB'}
%
%       % split operands delimited by '+'
%       ts = strsplit('1+2+3+4', '+');
%       ts <- {'1', '2', '3', '4'}
%
%       % It still works if there are spaces surrounding the delimiter
%       ts = strsplit('1 + 2 + 3 + 4', '+');
%       ts <- {'1', '2', '3', '4'}
%
%       % Consecutive delimiters results in empty terms
%       ts = strsplit('C,Java, C++ ,, Python, MATLAB', ',');
%       ts <- {'C', 'Java', 'C++', '', 'Python', 'MATLAB'}
%
%       % When no delimiter is presented, the entire string is considered
%       % as a single term
%       ts = strsplit('YouAndMe');
%       ts <- {'YouAndMe'}
%

%   History
%   -------
%       - Created by Dahua Lin, on Oct 9, 2008
%

    assert(ischar(s) && ndims(s) == 2 && size(s,1) <= 1, ...
        'strsplit:invalidarg', ...
        'The first input argument should be a char string.');

    if nargin < 2
        by_space = true;
    else
        d = delimiter;
        assert(ischar(d) && ndims(d) == 2 && size(d,1) == 1 && ~isempty(d), ...
            'strsplit:invalidarg', ...
            'The delimiter should be a non-empty char string.');

        d = strtrim(d);
        by_space = isempty(d);
    end

    s = strtrim(s);

    if by_space
        w = isspace(s);            
        if any(w)
            % decide the positions of terms        
            dw = diff(w);
            sp = [1, find(dw == -1) + 1];     % start positions of terms
            ep = [find(dw == 1), length(s)];  % end positions of terms

            % extract the terms        
            nt = numel(sp);
            terms = cell(1, nt);
            for i = 1 : nt
                terms{i} = s(sp(i):ep(i));
            end                
        else
            terms = {s};
        end

    else    
        p = strfind(s, d);
        if ~isempty(p)        
            % extract the terms        
            nt = numel(p) + 1;
            terms = cell(1, nt);
            sp = 1;
            dl = length(delimiter);
            for i = 1 : nt-1
                terms{i} = strtrim(s(sp:p(i)-1));
                sp = p(i) + dl;
            end         
            terms{nt} = strtrim(s(sp:end));
        else
            terms = {s};
        end        
    end
end
