function output = sql_generateSQLQuery(table, type, data, whereStatement)
%SQL_GENERATESQLQUERY Easily generate complex queries based on data structures
%   

    if nargin == 3
        whereStatement = [];
    end

    output = struct;
    output.success = true;
    output.status = 'Query could not be generated for an unknown reason';
    output.query = '';
    
    switch type
        case 'INSERT'
            [query success] = generateINSERT(table, data);
            output = validate(output, query, success);
        case 'UPDATE'
            [query success] = generateUPDATE(table, data, whereStatement);
            output = validate(output, query, success);
    end

end


function [query success] = generateINSERT(table, data)

    success = true;
    fnames = fieldnames(data);
    values = generateValuesINSERT(data);
    
    query = ['INSERT INTO ' table ' (' strjoin(fnames,',') ') '...
        'VALUES (' values ')'];

end


function [query success] = generateUPDATE(table, data, whereStatement)

    success = true;
    setStatement = generateValuesUPDATE(data);
    
    
    if ~isempty(whereStatement)
        whereStatement = [' WHERE ' whereStatement];
    else
        whereStatement = '';
    end
    
    query = ['UPDATE ' table ' SET ' setStatement whereStatement ';'];

end


function values = generateValuesINSERT(data)
    data = struct2cell(data);
    values = '';
    
    for ii = 1:numel(data)
        if ii ~= 1
            values = [values ','];
        end
        
        switch class(data{ii})
            case 'char'
                values = [values '''' data{ii} ''''];
            case 'double'
                if isnan(data{ii})
                    values = [values 'NULL'];
                else
                    values = [values num2str(data{ii})];
                end
        end
    end
end


function setStatement = generateValuesUPDATE(data)
    fnames = fieldnames(data);
    setStatement = '';
    
    for ii = 1:numel(fnames)
        if ii ~= 1
            setStatement = [setStatement ', '];
        end
        
        value = data.(fnames{ii});
        switch class(value)
            case 'char'
                valueString = ['''' value ''''];
            case 'double'
                if isnan(value)
                    valueString = 'NULL';
                else
                    valueString = ['''' num2str(value) ''''];
                end
        end
            
        setStatement = [setStatement fnames{ii} ' = ' valueString];
    end

%     data = struct2cell(data);
%     values = '';
%     
%     for ii = 1:numel(data)
%         if ii ~= 1
%             values = [values ','];
%         end
%         
%         
%     end
end


function output = validate(input, query, success)
    output = input;
    if success
        output.query = query;
        output.success = true;
        output.status = ''; 
    end
end
